# LoginBootstrapCodeigniterMySQL
Un sistema de ejemplo para aprender a utilizar Codeigniter, Bootstrap y MySQL. En este ejemplo, es un inicio de sesión, validando únicamente correo electrónico.

## Requerimientos

- Bootstrap 4.0.0
- PHP 7.2.3 (LAMPP)
- Codeigniter 3.1.7
- HASH lib para las contraseñas

## Instalación
La Instalación es simple, seguir éstos pasos.
- Descargar o clonar este proyecto.
- Copiarlo a htdocs de XAMPP, LAMP, WAMPP.
- Crear la base de datos con el script que viene el proyecto.
- Configurar las contraseñas de su respectivo servidor web.
- Ir a la URL http://localhost/login_bd_work_ci/

## Contribución

Cualquier ayuda para mejorar éste ejemplo, es bienvenidad. Como mencioné, es de ejemplo, cuando me encontraba probando el framework Codeigniter.

## Capturas

![Inicio](https://2.bp.blogspot.com/-T1lVTX_1WYo/WsWUMHDJv_I/AAAAAAAAE8Y/M9cFAQ8GU3MWkaH9EGBSyqKodDqYNaGKwCLcBGAs/s1600/Screenshot%2Bfrom%2B2018-04-04%2B22-09-20.png)

![Inicio de sesión](https://4.bp.blogspot.com/-cjwvc3W4q-o/WsWVZf4S7oI/AAAAAAAAE8k/yQdsAaPV6eIRipGu-Gz0eMZPgqmRHVugACLcBGAs/s1600/Screenshot%2Bfrom%2B2018-04-04%2B22-09-28.png)

![Registro de usuarios](https://1.bp.blogspot.com/-yEgxjRboJwk/WsWVcdv-QzI/AAAAAAAAE8o/8_q8YPaQ6hU6LmvFzxxN2_hmHrXI-0JIwCLcBGAs/s1600/Screenshot%2Bfrom%2B2018-04-04%2B22-09-31.png)

![Dashboard](https://4.bp.blogspot.com/-Lh9YbXYaOuY/WsWVeX8IQwI/AAAAAAAAE8w/mLf3p5ZnzYMV0uxGMiHFPlgs59xumCqmgCLcBGAs/s1600/Screenshot%2Bfrom%2B2018-04-04%2B22-10-56.png)
