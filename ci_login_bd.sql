-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 05-04-2018 a las 04:48:18
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: ci_login_bd
--
CREATE DATABASE IF NOT EXISTS ci_login_bd DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE ci_login_bd;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla users
--

DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users (
    id_user int(11) NOT NULL AUTO_INCREMENT,
    nom_user varchar(60) NOT NULL,
    email varchar(100) NOT NULL,
    password varchar(512) NOT NULL,
    PRIMARY KEY (id_user)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla users
--

INSERT INTO users (id_user, nom_user, email, password) VALUES
(7, 'LiNuXiToS', 'linuxitos@linuxgx.com', '$2y$10$7GruSS4SefPLiqg69GQWOeCAt6x4g2CB1H6k9ZM.2wNC4BA3a52sS');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
